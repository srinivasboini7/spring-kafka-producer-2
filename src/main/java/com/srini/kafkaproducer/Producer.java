package com.srini.kafkaproducer;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class Producer {

    private final KafkaTemplate<String,String> kafkaTemplate ;
    private final ResourceLoader resourceLoader;

    public void send(){
        Resource resource = resourceLoader.getResource("classpath:bank-sheet1.csv") ;

        try (CSVReader reader = new CSVReader(new FileReader(resource.getFile()))) {
            List<String[]> records = reader.readAll() ;

            Map<String, String> map = records
                    .stream()
                    .collect(Collectors.toMap(e-> e[0], Arrays::toString, (a, b)-> a) );

           log.info("{}",map);
           log.info("map size {}",map.size());

            map.entrySet()
                    .stream()
                    .map(e -> new ProducerRecord("test-topic-1", e.getKey(),e.getValue()))
                    .forEach(kafkaTemplate::send);


        } catch (IOException | CsvException e) {
            throw  new RuntimeException(e.getMessage()) ;
        }
    }
}
