package com.srini.kafkaproducer;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
@RequiredArgsConstructor
public class KafkaProducer2Application implements CommandLineRunner {

	private final Producer producer ;
	public static void main(String[] args) {
		SpringApplication.run(KafkaProducer2Application.class, args);
	}


	@Override
	public void run(String... args) throws Exception {

		producer.send();

	}

}
